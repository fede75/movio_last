

(function() {
	
	tinymce.PluginManager.requireLangPack('unipd3');

	tinymce.create('tinymce.plugins.Unipd3Plugin', {
		
		init : function(ed, url) {
			this.editor = ed;
		
			ed.addCommand('mceUnipd3', function() {
				ed.windowManager.open({
					file : url + '/dialog3.htm',
					width : 500,
					height : 460,
					inline : 1
				}, {
					plugin_url : url 
				});
			});
			ed.addCommand('UnipdResize',function(ui,v) {
    			ed.windowManager.params.mce_height = v.height + 16; 
			});

			
			ed.addButton('unipd3', {title : 'unipd3.desc', cmd : 'mceUnipd3', image : url + '/img/ricerca.png' });
		},
		
		getInfo : function() {
			return {
				longname : 'Unipd3 plugin',
				author : 'Federico Rizzato',
				authorurl : '',
				infourl : '',
				version : "1.0"
			};
		}
	});

	
	tinymce.PluginManager.add('unipd3', tinymce.plugins.Unipd3Plugin);
})(tinymce);
