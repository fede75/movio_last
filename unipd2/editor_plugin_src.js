
(function() {
	
	tinymce.PluginManager.requireLangPack('unipd2');

	tinymce.create('tinymce.plugins.Unipd2Plugin', {
		
		init : function(ed, url) {
			
			ed.addCommand('mceUnipd2', function() {
				ed.windowManager.open({
					file : url + '/dialog2.html',
					width : 320 + parseInt(ed.getLang('unipd2.delta_width', 0)),
					height : 120 + parseInt(ed.getLang('unipd2.delta_height', 0)),
					inline : 1
				}, {
					plugin_url : url, 
					some_custom_arg : 'custom arg' 
				});
			});

			
			ed.addButton('unipd2', {
				title : 'unipd2.desc',
				cmd : 'mceUnipd2',
				image : url + '/img/phaidra.png'
			});

			
			ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('unipd2', n.nodeName == 'IMG');
			});
		},

		
		createControl : function(n, cm) {
			return null;
		},

		
		getInfo : function() {
			return {
				longname : 'Unipd2 plugin',
				author : 'Some author',
				authorurl : 'http://tinymce.moxiecode.com',
				infourl : 'http://wiki.moxiecode.com/index.php/TinyMCE:Plugins/unipd2',
				version : "1.0"
			};
		}
	});

	
	tinymce.PluginManager.add('unipd2', tinymce.plugins.Unipd2Plugin);
})();